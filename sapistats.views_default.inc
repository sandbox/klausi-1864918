<?php
/**
 * @file
 * Search API Statistics Entity Views definitions
 */

/**
 * Implements hook_views_default_views().
 */
function sapistats_views_default_views() {
  $view = new view();
  $view->name = 'sapistats_admin_log';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'sapistats';
  $view->human_name = 'Search API Statistics Admin Log';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Search API Statistics Admin Log';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '15';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'sindex' => 'sindex',
    'type' => 'type',
    'value' => 'value',
  );
  $handler->display->display_options['style_options']['default'] = 'id';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sindex' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['summary'] = 'Search API Queries overview';
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: Search API Statistics query: Search api statistics query ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'sapistats';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['id']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['id']['format_plural'] = 0;
  /* Field: Search API Statistics query: Field */
  $handler->display->display_options['fields']['field']['id'] = 'field';
  $handler->display->display_options['fields']['field']['table'] = 'sapistats';
  $handler->display->display_options['fields']['field']['field'] = 'field';
  $handler->display->display_options['fields']['field']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field']['hide_alter_empty'] = 1;
  /* Field: Search API Statistics query: Ip */
  $handler->display->display_options['fields']['ip']['id'] = 'ip';
  $handler->display->display_options['fields']['ip']['table'] = 'sapistats';
  $handler->display->display_options['fields']['ip']['field'] = 'ip';
  $handler->display->display_options['fields']['ip']['exclude'] = TRUE;
  $handler->display->display_options['fields']['ip']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['external'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['ip']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['ip']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['ip']['alter']['html'] = 0;
  $handler->display->display_options['fields']['ip']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['ip']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['ip']['hide_empty'] = 0;
  $handler->display->display_options['fields']['ip']['empty_zero'] = 0;
  $handler->display->display_options['fields']['ip']['hide_alter_empty'] = 1;
  /* Field: Search API Statistics query: Sid */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['sid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['sid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['sid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['sid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['sid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['sid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['sid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['sid']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['sid']['separator'] = '';
  $handler->display->display_options['fields']['sid']['format_plural'] = 0;
  /* Field: Search API Statistics query: Sindex */
  $handler->display->display_options['fields']['sindex']['id'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sindex']['field'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['sindex']['alter']['path'] = 'admin/config/search/search_api/index/[sindex]';
  $handler->display->display_options['fields']['sindex']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['external'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['alt'] = '[sindex]';
  $handler->display->display_options['fields']['sindex']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['sindex']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['sindex']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['html'] = 0;
  $handler->display->display_options['fields']['sindex']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['sindex']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['sindex']['hide_empty'] = 0;
  $handler->display->display_options['fields']['sindex']['empty_zero'] = 0;
  $handler->display->display_options['fields']['sindex']['hide_alter_empty'] = 1;
  /* Field: Search API Statistics query: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'sapistats';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['external'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['timestamp']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['timestamp']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['timestamp']['alter']['html'] = 0;
  $handler->display->display_options['fields']['timestamp']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['timestamp']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['timestamp']['hide_empty'] = 0;
  $handler->display->display_options['fields']['timestamp']['empty_zero'] = 0;
  $handler->display->display_options['fields']['timestamp']['hide_alter_empty'] = 1;
  /* Field: Search API Statistics query: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'sapistats';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['type']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['type']['hide_alter_empty'] = 1;
  /* Field: Search API Statistics query: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'sapistats';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['uid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['uid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['uid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['uid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['uid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['uid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['uid']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['uid']['format_plural'] = 0;
  /* Field: Search API Statistics query: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'sapistats';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['value']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['value']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['value']['alter']['external'] = 0;
  $handler->display->display_options['fields']['value']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['value']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['value']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['value']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['value']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['value']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['value']['alter']['html'] = 0;
  $handler->display->display_options['fields']['value']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['value']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['value']['hide_empty'] = 0;
  $handler->display->display_options['fields']['value']['empty_zero'] = 0;
  $handler->display->display_options['fields']['value']['hide_alter_empty'] = 1;
  /* Filter criterion: Search API Statistics query: Sindex */
  $handler->display->display_options['filters']['sindex']['id'] = 'sindex';
  $handler->display->display_options['filters']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['filters']['sindex']['field'] = 'sindex';
  $handler->display->display_options['filters']['sindex']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sindex']['expose']['operator_id'] = 'sindex_op';
  $handler->display->display_options['filters']['sindex']['expose']['label'] = 'Sindex';
  $handler->display->display_options['filters']['sindex']['expose']['operator'] = 'sindex_op';
  $handler->display->display_options['filters']['sindex']['expose']['identifier'] = 'sindex';
  $handler->display->display_options['filters']['sindex']['expose']['required'] = 0;
  $handler->display->display_options['filters']['sindex']['expose']['multiple'] = FALSE;
  /* Filter criterion: Search API Statistics query: Field */
  $handler->display->display_options['filters']['field']['id'] = 'field';
  $handler->display->display_options['filters']['field']['table'] = 'sapistats';
  $handler->display->display_options['filters']['field']['field'] = 'field';
  $handler->display->display_options['filters']['field']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field']['expose']['operator_id'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['label'] = 'Field';
  $handler->display->display_options['filters']['field']['expose']['operator'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['identifier'] = 'field';
  $handler->display->display_options['filters']['field']['expose']['required'] = 0;
  $handler->display->display_options['filters']['field']['expose']['multiple'] = FALSE;
  /* Filter criterion: Search API Statistics query: Value */
  $handler->display->display_options['filters']['value']['id'] = 'value';
  $handler->display->display_options['filters']['value']['table'] = 'sapistats';
  $handler->display->display_options['filters']['value']['field'] = 'value';
  $handler->display->display_options['filters']['value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['value']['expose']['operator_id'] = 'value_op';
  $handler->display->display_options['filters']['value']['expose']['label'] = 'Keyword / Value';
  $handler->display->display_options['filters']['value']['expose']['operator'] = 'value_op';
  $handler->display->display_options['filters']['value']['expose']['identifier'] = 'keywordvalue';
  $handler->display->display_options['filters']['value']['expose']['required'] = 0;
  $handler->display->display_options['filters']['value']['expose']['multiple'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'field' => 'field',
    'ip' => 'ip',
    'sid' => 'sid',
    'sindex' => 'sindex',
    'timestamp' => 'timestamp',
    'type' => 'type',
    'uid' => 'uid',
    'value' => 'value',
  );
  $handler->display->display_options['style_options']['default'] = 'sid';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ip' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sindex' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'timestamp' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['summary'] = 'Search API Queries overview';
  $handler->display->display_options['style_options']['empty_table'] = 0;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'admin/config/search/sapistats/admin-log';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Admin Log';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'sindex' => 'sindex',
    'type' => 'type',
    'value' => 'value',
  );
  $handler->display->display_options['style_options']['default'] = 'id';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sindex' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['summary'] = 'Search API Queries overview';
  $handler->display->display_options['style_options']['empty_table'] = 0;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Search API Statistics query: Search api statistics query ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'sapistats';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['group_type'] = 'count';
  $handler->display->display_options['fields']['id']['label'] = 'Count';
  $handler->display->display_options['fields']['id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['id']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['id']['separator'] = '';
  $handler->display->display_options['fields']['id']['format_plural'] = 0;
  /* Field: Search API Statistics query: Sindex */
  $handler->display->display_options['fields']['sindex']['id'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sindex']['field'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['sindex']['alter']['path'] = 'admin/config/search/search_api/index/[sindex]';
  $handler->display->display_options['fields']['sindex']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['external'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['alt'] = '[sindex]';
  $handler->display->display_options['fields']['sindex']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['sindex']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['sindex']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['sindex']['alter']['html'] = 0;
  $handler->display->display_options['fields']['sindex']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['sindex']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['sindex']['hide_empty'] = 0;
  $handler->display->display_options['fields']['sindex']['empty_zero'] = 0;
  $handler->display->display_options['fields']['sindex']['hide_alter_empty'] = 1;
  /* Field: Search API Statistics query: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'sapistats';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['type']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['type']['hide_alter_empty'] = 1;
  /* Field: Search API Statistics query: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'sapistats';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['value']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['value']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['value']['alter']['external'] = 0;
  $handler->display->display_options['fields']['value']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['value']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['value']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['value']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['value']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['value']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['value']['alter']['html'] = 0;
  $handler->display->display_options['fields']['value']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['value']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['value']['hide_empty'] = 0;
  $handler->display->display_options['fields']['value']['empty_zero'] = 0;
  $handler->display->display_options['fields']['value']['hide_alter_empty'] = 1;
  $handler->display->display_options['path'] = 'admin/config/search/sapistats/top-keywords';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Top Keywords Chart';
  $handler->display->display_options['menu']['description'] = 'Top Search Keywords Chart';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $export['sapistats'] = $view;

  return $export;
}
