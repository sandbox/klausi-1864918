<?php

/**
 * Implements hook_views_data_alter().
 */
function sapistats_aggregation_views_data_alter(&$data) {
  // Adds possibility to add relation to reference filter in views.
  $data['taxonomy_term_data']['table']['join']['datastore'] = array(
    'left_field' => 'reference',
    'field' => 'tid',
  );
}
