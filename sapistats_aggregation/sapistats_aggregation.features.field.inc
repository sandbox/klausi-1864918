<?php
/**
 * @file
 * sapistats_aggregation.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function sapistats_aggregation_field_default_fields() {
  $fields = array();

  // Exported field: 'datastore-sapistats_ds_job-datastore_field_sapistats_ds_job'.
  $fields['datastore-sapistats_ds_job-datastore_field_sapistats_ds_job'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'datastore_field_sapistats_ds_job',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'datastore_field',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'datastore_field',
    ),
    'field_instance' => array(
      'bundle' => 'sapistats_ds_job',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'datastore_field',
          'settings' => array(),
          'type' => 'datastore_field_default_formatter',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'datastore',
      'field_name' => 'datastore_field_sapistats_ds_job',
      'label' => 'datastore_field_sapistats_ds_job',
      'required' => FALSE,
      'settings' => array(
        'multiple' => TRUE,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'datastore_field',
        'settings' => array(),
        'type' => 'datastore_field_widget_hidden',
        'weight' => '0',
      ),
    ),
  );

  // Exported field: 'datastore-sapistats_ds_job-field_sapistats_granularity'.
  $fields['datastore-sapistats_ds_job-field_sapistats_granularity'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_sapistats_granularity',
      'field_permissions' => array(
        'type' => '0',
      ),
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'd' => 'daily',
          'w' => 'weekly',
          'm' => 'monthly',
        ),
        'allowed_values_function' => '',
        'profile2_private' => FALSE,
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'sapistats_ds_job',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => 1,
        ),
      ),
      'entity_type' => 'datastore',
      'field_name' => 'field_sapistats_granularity',
      'label' => 'Granularity',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'datastore-sapistats_ds_job-field_sapistats_query_type'.
  $fields['datastore-sapistats_ds_job-field_sapistats_query_type'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_sapistats_query_type',
      'field_permissions' => array(
        'type' => '0',
      ),
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'filter' => 'Filter/Facet',
          'keyword' => 'Keyword/Full-text',
        ),
        'allowed_values_function' => '',
        'profile2_private' => FALSE,
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'sapistats_ds_job',
      'default_value' => array(
        0 => array(
          'value' => 'keyword',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => 2,
        ),
      ),
      'entity_type' => 'datastore',
      'field_name' => 'field_sapistats_query_type',
      'label' => 'Query Type',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '2',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Granularity');
  t('Query Type');
  t('datastore_field_sapistats_ds_job');

  return $fields;
}
