Module closely integrates with Search and Facet APIs, 
it logs search queries (keywords and facets) and provides site maintainers
with a visual representation of the collected data, which can be shown as
a simple list in a block or as a chart.
It integrates here with the Views module.
Some usage-scenarios:

How many results did a search facet have yesterday?
How many 30 days ago?
How did the popularity of a facet evolve over time?
Do people actually use the facets I’m exposing on my site?
How often has this particular facet been clicked on today?
How often is the whole search used anyway in a given time period?
What are the most trending search terms compared to last week?
Which facet has been relegated or is used less often?

A user will be able to set up the time period for which data will be stored.
Granularity of reports will be also adjustable.
This data after evaluation could be published on the site,
for instance the most searched jobs may be listed on a job search portal.
